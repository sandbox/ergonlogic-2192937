<?php

/**
 * @file
 * An implementation of the Checklist API.
 */

/**
 * Implements hook_checklistapi_checklist_info().
 *
 * Defines a checklist based on best practices for Aegir platform management.
 * @link http://community.aegirproject.org/node/20 @endlink
 * .
 */
function praxis_platform_checklist_checklistapi_checklist_info() {
  $definitions = array();
  $definitions['praxis_platform_checklist'] = array(
    '#title' => t('Praxis Platform Checklist'),
    '#path' => 'admin/config/development/platform-checklist',
    '#description' => t('Checklist for Aegir platform management.'),
    '#help' => t('<p>This checklist based on best practices for platform management, as documented here: <a>some doc links here</a>.</p>'),
    'preparation' => array(
      '#title' => t('Preparation'),
      '#description' => t('Prior to starting with upgrades, we should take some steps to ensure we don\'t lose data.</p>'),
      'check_git' => array(
        '#title' => t('Check that we don\'t have any un-committed changes to our platforms makefile.'),
        'handbook_page' => array(
          '#text' => t('Git guide'),
          '#path' => 'http://drupal.org/documentation/install',
        ),
      ),
      'git_pull' => array(
        '#title' => t('Ensure our repository is up-to-date.'),
        'handbook_page' => array(
          '#text' => t('Git guide'),
          '#path' => 'http://drupal.org/node/306808',
        ),
      ),
      'check_profile_updates' => array(
        '#title' => t('Check for updates to the install profiles on the platform'),
        'handbook_page' => array(
          '#text' => t('Working with blocks (content in regions)'),
          '#path' => 'http://drupal.org/documentation/modules/block',
        ),
      ),
      'check_custom_modules' => array(
        '#title' => t('Check custom modules for updates'),
        'handbook_page' => array(
          '#text' => t('Managing users'),
          '#path' => 'http://drupal.org/node/627158',
        ),
      ),
      'check_contrib_packages' => array(
        '#title' => t('Check whether contrib themes and modules need updating'),
        'handbook_page' => array(
          '#text' => t('Installing modules and themes'),
          '#path' => 'http://drupal.org/documentation/install/modules-themes',
        ),
      ),
    ),
    'makefiles' => array(
      '#title' => t('Update makefiles'),
      '#description' => t('<p>Update and test makefiles according to the findings of prior steps.</p>'),
      'build' => array(
        '#title' => t('Build a new platform'),
        'new_platform' => array(
          '#text' => t('Upgrading from previous versions'),
          '#path' => 'http://drupal.org/upgrade',
        ),
        'import' => array(
          '#text' => t('Import the new platform into the Aegir front-end'),
          '#path' => 'http://drupal.org/patch/apply',
        ),
        '' => array(
          '#text' => t('Security advisories'),
          '#path' => 'http://drupal.org/security',
        ),
        'handbook_page_monitoring' => array(
          '#text' => t('Monitoring a site'),
          '#path' => 'http://drupal.org/node/627162',
        ),
      ),
      'navigation_menus_taxonomy' => array(
        '#title' => t('Navigation, menus, taxonomy'),
        'handbook_page_menus' => array(
          '#text' => t('Working with Menus'),
          '#path' => 'http://drupal.org/documentation/modules/menu',
        ),
        'handbook_page_taxonomy' => array(
          '#text' => t('Organizing content with taxonomy'),
          '#path' => 'http://drupal.org/documentation/modules/taxonomy',
        ),
      ),
      'locale_i18n' => array(
        '#title' => t('Locale and internationalization'),
        'handbook_page' => array(
          '#text' => t('Multilingual Guide'),
          '#path' => 'http://drupal.org/documentation/multilingual',
        ),
      ),
      'customize_front_page' => array(
        '#title' => t('Drastically customize front page'),
        'handbook_page' => array(
          '#text' => t('Totally customize the LOOK of your front page'),
          '#path' => 'http://drupal.org/node/317461',
        ),
      ),
      'theme_modification' => array(
        '#title' => t('Theme and template modifications'),
        'handbook_page' => array(
          '#text' => t('Theming Guide'),
          '#path' => 'http://drupal.org/documentation/theme',
        ),
      ),
    ),
    'migrations' => array(
      '#title' => t('Perform site migrations'),
      'contribute_docs_support' => array(
        '#title' => t('Contributing documentation and support'),
        'handbook_page_docs' => array(
          '#text' => t('Contribute to documentation'),
          '#path' => 'http://drupal.org/contribute/documentation',
        ),
        'handbook_page_support' => array(
          '#text' => t('Provide online support'),
          '#path' => 'http://drupal.org/contribute/support',
        ),
      ),
      'content_types_views' => array(
        '#title' => t('Content types and views'),
        'handbook_page_content_types' => array(
          '#text' => t('Working with nodes, content types and fields'),
          '#path' => 'http://drupal.org/node/717120',
        ),
        'handbook_page_views' => array(
          '#text' => t('Working with Views'),
          '#path' => 'http://drupal.org/documentation/modules/views',
        ),
      ),
      'actions_workflows' => array(
        '#title' => t('Actions and workflows'),
        'handbook_page' => array(
          '#text' => t('Actions and Workflows'),
          '#path' => 'http://drupal.org/node/924538',
        ),
      ),
      'development' => array(
        '#title' => t('Theme and module development'),
        'handbook_page_theming' => array(
          '#text' => t('Theming Guide'),
          '#path' => 'http://drupal.org/documentation/theme',
        ),
        'handbook_page_development' => array(
          '#text' => t('Develop for Drupal'),
          '#path' => 'http://drupal.org/documentation/develop',
        ),
      ),
      'advanced_tasks' => array(
        '#title' => t('jQuery, Form API, security audits, performance tuning'),
        'handbook_page_jquery' => array(
          '#text' => t('JavaScript and jQuery'),
          '#path' => 'http://drupal.org/node/171213',
        ),
        'handbook_page_form_api' => array(
          '#text' => t('Form API'),
          '#path' => 'http://drupal.org/node/37775',
        ),
        'handbook_page_security' => array(
          '#text' => t('Securing your site'),
          '#path' => 'http://drupal.org/security/secure-configuration',
        ),
        'handbook_page_performance' => array(
          '#text' => t('Managing site performance'),
          '#path' => 'http://drupal.org/node/627252',
        ),
      ),
      'contribute_code' => array(
        '#title' => t('Contributing code, designs and patches back to Drupal'),
        'handbook_page' => array(
          '#text' => t('Contribute to development'),
          '#path' => 'http://drupal.org/contribute/development',
        ),
      ),
      'professional' => array(
        '#title' => t('Drupal consultant or working for a Drupal shop'),
      ),
      'chx_or_unconed' => array(
        '#title' => t(
          "I'm a !chx or !UnConeD.",
          array(
            '!chx' => l(t('chx'), 'http://drupal.org/user/9446'),
            '!UnConeD' => l(t('UnConeD'), 'http://drupal.org/user/10'),
          )
        ),
      ),
    ),
  );
  return $definitions;
}
